package edu.uprm.cse.datastructures.cardealer.model;

import java.util.Comparator;

public class CarComparator implements Comparator <Car> { 
	
	@Override 
	public int compare(Car c1, Car c2){  
		
		String c1String = c1.getCarBrand() + c1.getCarModel() + c1.getCarModelOption(); 
		String c2String = c2.getCarBrand() + c2.getCarModel() + c2.getCarModelOption();
		
	    if (c1String == c2String) 
	        return 0;
	    
	    if (c1String == null) 
	    	return -1;
	    
	    if (c2String == null) 
	        return 1;
	    
	    return c1String.compareTo(c2String);
	  }

}
