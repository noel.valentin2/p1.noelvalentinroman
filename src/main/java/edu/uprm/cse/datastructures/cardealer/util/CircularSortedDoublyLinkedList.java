package edu.uprm.cse.datastructures.cardealer.util;

import java.util.Comparator;
import java.util.Iterator;

public class CircularSortedDoublyLinkedList<E> implements SortedList <E>{ 
	 
	private Node<E> header; 
	private int size;  
	private Comparator<E> comp;
	
	//Constructors 
	public CircularSortedDoublyLinkedList(Comparator<E> cmp) { 
		comp = cmp;
		header = new Node<E>();
		header.setNext(header); 
		header.setPrev(header); 
		size = 0;
	} 
	
	public boolean add(E obj) {                         
	 
		 Node<E> curr = header;
	     Node<E> elem = new Node<E>(obj);
	   
	     if(isEmpty()) { 
	    	 header.setNext(elem); 
	    	 header.setPrev(elem);
	    	 elem.setNext(header); 
	    	 elem.setPrev(header);
	    	 size++;
	     } 
	     
	     else { 
	    	 curr = header.getNext();
	    	 while(curr != header) { 
	    		 if(comp.compare(elem.getElement(), curr.getElement()) <= 0) { 
	    			 elem.setNext(curr); 
	    			 elem.setPrev(curr.getPrev());
	    			 curr.getPrev().setNext(elem);
	    			 curr.setPrev(elem); 
	    			 size++;
	    			 return true;
	    		 } 
	    		 else if(curr.getNext() != header)
	    			 curr = curr.getNext(); 
	    		 
	    		 else { 
	    			 elem.setPrev(header.getPrev());
	    			 elem.setNext(header); 
	    			 header.getPrev().setNext(elem);
	    			 header.setPrev(elem);
	    			 size++; 
	    			 return true;
	    		 }
	    	 }
	    		 
	     }
	     
	     return false;
	} 
	
	public int size() { 
		return size;
	}
	
	public boolean remove(E obj) {          //remove the first one that it finds
	       Node<E> curr = header.getNext();

	        while(curr != header) { 
	            if(curr.getElement().equals(obj)) {       
	                curr.getPrev().setNext(curr.getNext());
	                curr.getNext().setPrev(curr.getPrev()); 
	                curr.clearlinks();
	                size--; 
	                return true;
	            }
	            else 
	                curr = curr.getNext();
	        }
	        
	        return false;
	    } 
	
	public boolean remove(int index) { 
		Node<E> curr = header;  
    	
    	if(index < 0 || index >= size)
    		return false;
    	
    	for(int i = 0; i <= index; i++) { 
    		curr = curr.getNext(); 
    	}  
    	
    	curr.getPrev().setNext(curr.getNext());
        curr.getNext().setPrev(curr.getPrev()); 
        curr.clearlinks();
        size--;
    	
        return true;
    } 
	
	
	public int removeAll(E obj) { 
	   	int copiesRemoved = 0; 
    	Node<E> curr = header.getNext(); 
    	Node<E> ntbr = curr;
    	
    	while(curr != header) {
    		if(curr.getElement().equals(obj)) { 
    			copiesRemoved++; 
    			curr.getPrev().setNext(curr.getNext());
    			curr.getNext().setPrev(curr.getPrev());      
    			curr = curr.getNext(); 
    			ntbr.clearlinks();
    			size--; 
    			ntbr = curr;
    		} 
    		else { 
    			curr = curr.getNext(); 
    			ntbr = curr;
    		}
    	} 
    	
    	return copiesRemoved;
    }
	
	public E first() { 
		return (E) header.getNext().getElement(); 
	}  
	
	public E last() { 
		return (E) header.getPrev().getElement();
	}
	
	public E get(int index) { 
		Node<E> curr = header;
        for(int i = 0; i <= index; i++) { 
            curr = curr.getNext();
        }
        return curr.getElement();
    } 
	
	public void clear() {         
		Node<E> elf = header.getNext();
    	while(elf != header) { 
    		elf.getNext().setPrev(header);
    		header.setNext(elf.getNext()); 
    		elf.clearlinks(); 
    		elf = header.getNext();
    	}
    	
    	size = 0;
    }
	
	
	
	public boolean contains(E e) { 
		Node<E> curr = header.getNext(); 
        while(curr != header) { 
            if(curr.getElement().equals(e))     
                return true; 
            curr = curr.getNext();
        }    
        return false;
    } 
	
	
	public boolean isEmpty() { 
		return size == 0;
	} 
	
	public int firstIndex(E e) { 
		int index =0; 
    	Node<E> curr = header.getNext(); 
    	
    	while(curr.getNext() != header) { 
    		if(curr.getElement().equals(e)) {
    			return index;
    		} 
    			curr = curr.getNext(); 
    			index--;
    		
    	} 
    	return -1;
    } 
    
	
	public int lastIndex(E e) { 
		int index = size-1; 
    	Node<E> curr = header.getPrev(); 
    	
    	while(curr.getPrev() != header) { 
    		if(curr.getElement().equals(e)) {
    			return index;
    		} 
    			curr = curr.getPrev(); 
    			index--;
    		
    	} 
    	return -1;
    }


	@Override
	public Iterator<E> iterator() {                
		// TODO Auto-generated method stub
		return null;
	}
	
	
	
	private static class Node<E> { 
		
		private E element; 
		private Node<E> prev,next; 
		
		
		//Constructors
		public Node(){} 
		
		public Node(E elem) { 
			element = elem;
		} 
		
		public Node(E elem, Node<E> pre, Node<E> ne){ 
			element = elem; 
			prev = pre; 
			next = ne;
		} 
		
		//Methods 
		public E getElement() {
			return element;
		} 
		
		public Node<E> getPrev() { 
			return prev;
		} 
		
		public Node<E> getNext() { 
			return next;
		} 
		
		public void setElement(E e) { 
			element = e;
		} 
		
		public void setPrev(Node p){ 
			prev = p;
		} 
		
		public void setNext(Node n) { 
			next = n;
		} 
		
		public void clearlinks() { 
			
			next = null; 
			prev = null;
		}
	}
	

}
